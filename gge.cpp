///////////////////////////////////////////////////////////////////////////////
///////// University of Hawaii, College of Engineering
///////// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////////
///////// @file catPower.cpp
///////// @version 1.0
/////////
///////// @author Creel Patrocinio <creel@hawaii.edu>
///////// @date 02/16/2022
/////////////////////////////////////////////////////////////////////////////////////

#include "gge.h"

double fromGasolineGallonEquivalentToJoule( double gasolinegallonequivalent ) {
      return gasolinegallonequivalent / GASOLINEGALLONEQUIVALENTS_IN_A_JOULE;
}
double fromJouleToGasolineGallonEquivalent( double joule ) {
      return joule * GASOLINEGALLONEQUIVALENTS_IN_A_JOULE;
}


