///////////////////////////////////////////////////////////////////////////////
/////////// University of Hawaii, College of Engineering
/////////// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///////////
/////////// @file catPower.cpp
/////////// @version 1.0
///////////
/////////// @author Creel Patrocinio <creel@hawaii.edu>
/////////// @date 02/16/2022
///////////////////////////////////////////////////////////////////////////////////////
//

#include "foe.h"

double fromFoeToJoule( double foe ) {
      return foe / FOE_IN_A_JOULE;
}
double fromJouleToFoe( double joule ) {
      return joule * FOE_IN_A_JOULE;
}



