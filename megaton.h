
///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file catPower.cpp
///// @version 1.0
/////
///// @author Creel Patrocinio <creel@hawaii.edu>
///// @date 02/16/2022
/////////////////////////////////////////////////////////////////////////////////
//
//
//
#pragma once


const double MEGATON_IN_A_JOULE = 1/(4.18e15) ;
const char MEGATON = 'm';
extern double fromMegatonToJoule( double megaton ) ;
extern double fromJouleToMegaton( double joule ) ;




