/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file cat.h
///// @version 1.0
/////
///// @author Creel Patrocinio <creel@hawaii.edu>
///// @date 02/16/2022
///////////////////////////////////////////////////////////////////////////////


#pragma once

const double CATPOWER_IN_A_JOULE = 0;
const char CATPOWER = 'c';
extern double fromCatPowertoJoule(double catPower);
extern double fromJouleToCatPower(double joules);


